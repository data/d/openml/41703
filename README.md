# OpenML dataset: MIP-2016-classification

https://www.openml.org/d/41703

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

source: http://plato.asu.edu/ftp/solvable.html
authors: Rolf-David Bergdoll

PAR10 performances of modern solvers on the solvable instances of MIPLIB2010.
http://miplib.zib.de/

The algorithm runtime data was directly taken from the '12 threads' table of
H. Mittelmann's evaluations.

The features were generated using the MIP feature computation code from
http://www.cs.ubc.ca/labs/beta/Projects/EPMs/

To record runtimes of the feature computations, runsolver was used: 
http://www.cril.univ-artois.fr/~roussel/runsolver/

Part of Open Algorithm Challenge 2017 ("Mira").

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41703) of an [OpenML dataset](https://www.openml.org/d/41703). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41703/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41703/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41703/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

